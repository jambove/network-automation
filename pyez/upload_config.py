from jnpr.junos import Device
from jnpr.junos.utils.config import Config

dev = Device(host='192.168.169.131',user='root', password='Juniper1').open()

conf_file = "config.conf"
with Config(dev, mode='exclusive') as cu:
    cu.load(path=conf_file, overwrite=True, format= 'text')
    cu.pdiff()
    cu.commit()
    print("The changes were succesfully committed: ",cu.commit_check())

dev.close()
