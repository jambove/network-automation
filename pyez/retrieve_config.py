from jnpr.junos import Device
from lxml import etree

hostIP = '192.168.169.131'

dev = Device(host = hostIP, user = 'root', password = 'Juniper1')
print ('\n'+ 'Opening connection to ',hostIP,"\n")
dev.open()

#RETRIEVING AND ADJUSTING CONFIG INFORMATION
info = dev.rpc.get_config(options={'format': 'text'})
vartext = etree.tostring(info,encoding= 'unicode')
mytext = vartext[20:-22]

#WRITING TO A FILE
file = open("config.conf", "w+")
file.write(mytext)
file.close()
print("The configuration was written to the file\n")

dev.close()
