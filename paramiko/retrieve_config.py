import paramiko, time, os

try:
        sshClient = paramiko.SSHClient()
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print ('Trying to connect to 192.168.122.27')
        sshClient.connect('192.168.122.27', username='root', password='Juniper1')

        channel = sshClient.invoke_shell()
        print ('SRX shell invoked')
        time.sleep(2)

        os.system('clear')

        channel.send('cli\n')
        time.sleep(1)

        routerOutput = channel.recv(1000)
        channel.send('show configuration | no-more\n')
        time.sleep(0.7)

        output = channel.recv(10000)
        print(output)
        channel.close()

        outFile = open('SRXconf.cfg','w')
        outFile.write(output)
        outFile.close()

except Exception as  ex:
        print ('Something went wrong:')
        print(ex)

