# Week 48 Network Automation

#### Do the following tasks:  

* Install Ansible on Linux.  
* Test JSON and YAML.  
* Create a basic play book and execute it.   
    * Create and run the uptime.yaml version 1.1.  
    * Create and run the uptime.yaml version 1.2.  
    * Create and run the uptime.yaml version 1.3.  
    * Repeating a Playbook for Devices with Errors.  
* Set up Ansible logging to a file.   

#### Deliverables

* Make a Network diagram.  
* Demonstrate to the lecturer when a playbook is working.  



#### Sources

* Day One: Automating Junos with Ansible, 2nd Edition  



#### Requirements:

- Dependencies: (apt install openssh-client build-essential libffi-dev libxslt-dev libssl-dev python python-dev python-pip git)  
- Ansible version 2.8.6 (Newest verison does not work with Junos) (pip install ansible==2.8.6)  
- junos-eznc (pip install junos-eznc)  
- jxmlease (pip install jxmlease)    
- Juniper.junos (ansible-galaxy install Juniper.junos)   
(Run on a Debian 10 Stable system)

#### Notes:
- Newest Ansible version can be used with newest junos ansible modules  
https://docs.ansible.com/ansible/latest/search.html?q=junos&check_keywords=yes&area=default

# Week 49 Network Automation

#### Do the following tasks:  

* Test Junos, RPC, NETCONF, and XML SSH keys by    
creating basic play books and executing them:  
    * Create and run the uptime.yaml version 2.0 to 2.3.  
    * Create and run the Interfaces Version 1.0 to 1.4.  
    * Create and run the uptime.yaml version 3.0.   

#### Deliverables

* Make a Network diagram.  
* Demonstrate to the lecturer when playbooks 2.3, 1.4 and 3.0 are working.  

#### Sources

* Day One: Automating Junos with Ansible, 2nd Edition  
  - Chapter 5: Junos, RPC, NETCONF, and XML  
  - Chapter 6: Using SSH Keys  


#### Requirements:

- Dependencies: (apt install openssh-client build-essential libffi-dev libxslt-dev libssl-dev python python-dev python-pip git)  
- Ansible version 2.8.6 (Newest verison does not work with Junos) (pip install ansible==2.8.6)  
- junos-eznc (pip install junos-eznc)  
- jxmlease (pip install jxmlease)  
- Juniper.junos (ansible-galaxy install Juniper.junos)  
(Run on a Debian 10 Stable system)  

#### Notes:
- Newest Ansible version can be used with newest junos ansible modules
https://docs.ansible.com/ansible/latest/search.html?q=junos&check_keywords=yes&area=default

