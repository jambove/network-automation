from netmiko import ConnectHandler

#Devices definition
vsrx_a = {
    'device_type': 'juniper_junos',
    'host':   '192.168.169.131',
    'username': 'root',
    'password': 'Juniper1',
    'session_log': 'A_my_log.txt' #log session for troubleshooting
}
vsrx_b = {
    'device_type': 'juniper_junos',
    'host':   '192.168.169.136',
    'username': 'root',
    'password': 'Juniper1',
    'session_log': 'B_my_log.txt' #log session for troubleshooting
}

def read(filename):
    with open(filename) as file:
        data = file.read()
    return data

def write(filename,data):
    with open(filename, 'w+') as file:
        file.write(data)
    return filename

def main():
        #Initialising connections
        a_connect = ConnectHandler(**vsrx_a)
        b_connect = ConnectHandler(**vsrx_b)

        #Retrieving and storing configuration A
        output = a_connect.send_command("show configuration")
        filename = write("configuration-A.conf",output)
        print("Configuration A written to the file: ", filename)

        #Retrieving and storing configuration B
        output = b_connect.send_command("show configuration")
        filename = write("configuration-B.conf",output)
        print("Configuration B written to the file: ", filename)
        print()

        #Loading and commiting changes to the router A
        command = read("command-A.txt")
        a_connect.send_config_set(command, exit_config_mode= False)
        print(command)
        print("Router-A: ",a_connect.send_command("commit",expect_string="commit complete"))
        a_connect.disconnect()
        print("\n")

        #Loading and commiting changes to the router B
        command = read("command-B.txt")
        b_connect.send_config_set(command, exit_config_mode= False)
        print(command)
        print("Router-B: ",b_connect.send_command("commit",expect_string="commit complete"))
        b_connect.disconnect()
        print("\n")
main()
